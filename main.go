package main

import (
	"fmt"
	"net"

	"github.com/drael/GOnetstat"
)

var countOnly = map[string]bool{
	"ESTABLISHED": true,
	"CLOSE_WAIT":  true,
	"CLOSING":     true,
}

func main() {
	cidrs := []string{
		"169.254.0.0/16",
		"172.17.0.0/16",
		"172.18.0.0/16",
		"172.19.0.0/16",
		"172.20.0.0/16",
		"172.21.0.0/16",
		"172.29.0.0/16",
		"192.168.3.0/24",
		"192.168.3.0/24",
		"192.168.37.0/24",
		"192.168.39.0/24",
		"192.168.42.0/24",
	}

	nets := make([]*net.IPNet, 0)
	for _, c := range cidrs {
		_, net, err := net.ParseCIDR(c)
		if err != nil {
			panic(err)
		}

		if net == nil {
			panic("net is nil!")
		}

		nets = append(nets, net)
	}

	pps := map[string]func() []GOnetstat.Process{
		"tcp": GOnetstat.Tcp,
		"udp": GOnetstat.Udp,
	}

	buckets := make(map[string]int)

	for _, pp := range pps {
		for _, p := range pp() {
			if _, ok := countOnly[p.State]; !ok {
				continue
			}

			foreignIP := net.ParseIP(p.ForeignIp)

			skipLocal := false
			for _, n := range nets {
				if n.Contains(foreignIP) {
					skipLocal = true
					break
				}
			}

			if skipLocal {
				continue
			}

			key := fmt.Sprintf("%s-%d", p.ForeignIp, p.ForeignPort)

			_, ok := buckets[key]
			if !ok {
				buckets[key] = 0
			}

			buckets[key]++
		}
	}

	max := 0
	maxBucket := ""
	for b, no := range buckets {
		if no > max {
			max = no
			maxBucket = b
		}
	}

	fmt.Println(maxBucket, max)
}
